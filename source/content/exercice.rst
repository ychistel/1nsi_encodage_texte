Exercices
==========

.. exercice::

    On donne la table ASCII:

    .. figure:: ../img/tableASCII.png
        :alt: image
        :align: center
        :width: 560
        
    #.  Que remarque-t-on dans la table ASCII en observant les lettres majuscules et les lettres minuscules ?
    #.  Donner les codes binaires des lettres M, I, m, i.
    #.  Quelle transformation binaire peut-on faire pour transformer une majuscule en minuscule et inversement ?
        
.. exercice::

    Un caractère a comme point de code ``U+005A``.
    
    #.  A quelle plage appartient ce point de code ?
    #.  Combien d'octets faut-il pour l'encoder en UTF-8 ?
    #.	Ce caractère est-il un caractère de la table ASCII ? Justifier.

.. exercice::
    
    Le caractère de point de code ``U+FFFD`` est encodé en UTF-8 sur trois octets.
    
    #.  Convertir en binaire le point de code de ce caractère.
    #.  En déduire l'encodage de ce caractère en UTF-8, c'est à dire tel qu'il est encodé en machine.
    #.  Convertir l'encodage binaire en hexadécimal.

.. exercice::
    
    Dans la langue française, on retrouve des ligatures, c'est à dire que certaines lettres sont collées l'une à l'autre. Par exemple, le "o" et le "e" sont collés dans les mots "nœud", "œil" et "œuvre".
    
    La ligature œ est apparue dans la table ISO 8859-15 avec le code hexadécimal ``BD``. Dans la norme Unicode, cette ligature a pour point de code ``U+0153``. 
    
    On rappelle le codage binaire en UTF-8 selon la valeur du point de code dans le tableau ci-dessous.

    .. csv-table::
        :header: Point de code, Ecriture binaire
        :file: ../csv/table_unicode.csv
        :delim: &
        
    #.  Combien faut-il d’octets pour encoder la ligature "œ" en UTF-8 ?
    #.  Après avoir converti le point de code en binaire, en déduire le codage binaire en UTF-8 de la ligature "œ".
