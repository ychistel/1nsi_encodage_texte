﻿Représentation des caractères
=============================

La représentation des textes dans un ordinateur consiste à représenter chaque caractère du texte par un code binaire. Chaque caractère à son code binaire encodé sur 1 ou plusieurs octets.

On s'intéresse à 3 tables de caractères qui ont marqué l'informatique.

-  La table de codage ASCII créée dans les années 1960.
-  La norme UNICODE et ses représentations binaires

La table ASCII
---------------

La table ASCII (American Standard Code for Information Interchange) est la première table de caractères utilisée en informatique créée au début des années 1960.

Cette table définit un jeu de 128 caractères. Chaque caractère est encodé sur un octet mais seuls 7 bits sont utilisés, le bit de poids fort étant égal à 0. 

.. hint::
	
	On rappelle qu'un encodage sur 7 bits permet d'avoir :math:`2^{7}=128` caractères différents.

Chaque caractère de la table est repéré par un code hexadécimal et par un code décimal. 

.. admonition:: Exemple

	Le caractère ``A`` de la table ASCII est encodé :math:`01000001_{2}` en binaire. 
	
	Le caractère ``A`` est repéré par le code hexadécimal :math:`41_{16}` qui a pour valeur décimale :math:`65_{10}` .

La figure ci-dessous représente la table ASCII:

.. figure:: ../img/tableASCII.png
   :alt: tableASCII.png
   :width: 560
   :align: center

Cette table est à double entrée. La colonne de gauche représente les trois premiers chiffres hexadécimaux de son code et la première ligne le dernier chiffre hexadécimal du code du caractère.

On lit la table de la façon suivante:

-  on repère le caractère dans la table situé à l’intersection d’une ligne et d’une colonne;
-  sur la ligne de ce caractère, on note les trois chiffres hexadécimaux de la première colonne;
-  sur la colonne de ce caractère, on note le chiffre hexadécimal de la première ligne.
-  on concatène les trois chiffres de la première colonne avec le chiffre de la première ligne et on obtient le code hexadécimal du caractère ASCII.


La norme UNICODE
-----------------

La table de caractères ASCII est très insuffisante. Il manque par exemple tous les caractères accentués utilisés dans les langues latines. L'ISO (Organisation Internationale de normalisation) a donc défini un jeu universel de caractères sous la norme ISO 10646 appelée UNICODE.

Cette norme associe à chaque caractère un **point de code** et un **nom unique**. Ce point de code est écrit en hexadécimal préfixé par ``U+``.

.. admonition:: Exemple

	Le caractère "A" a pour point de code ``U+0041`` et nom unique **LATIN CAPITAL LETTER A**. 
	
	On remarque que c'est la valeur hexadécimale de ``A`` dans la table ASCII.

**Unicode** est une norme qui définit les techniques pour encoder en binaire les **points de code** de façon plus ou moins économique. Ces encodages sont appelés "Universal Transform Format" et notés ``UTF-n`` où n désigne le nombre minimal de bits pour représenter un point de code (n=8, 16 ou 32).

UTF-8 est le format d'encodage qui utilise au minimum 8 bits (1 octet) pour encoder les caractères. Il est compatible avec la table de caractères ASCII. Les 128 premiers points de code sont ceux de la table ASCII.

Selon les caractères et la valeur des points de code, l'encodage en UTF-8 utilise 1, 2, 3 ou 4 octets.

Le tableau ci-dessous donne l'encodage des caractères en UTF-8.

+------------------------+------------------------------------+
| Point de code          | Encodage en binaire                |
+========================+====================================+
| de U+0000 à U+007F     | 0xxxxxxx                           |
+------------------------+------------------------------------+
| de U+0080 à U+07FF     | 110xxxxx 10xxxxxx                  |
+------------------------+------------------------------------+
| de U+0800 à U+FFFF     | 1110xxxx 10xxxxxx 10xxxxxx         |
+------------------------+------------------------------------+
| de U+10000 à U+10FFFF  | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx|
+------------------------+------------------------------------+

.. admonition:: Exemple

	Le caractère accentué "É" a pour point de code ``U+00C9``. Ce point de code est compris entre ``U+0080`` et ``U+07FF`` donc il est encodé en UTF-8 sur 2 octets.

	L'encodage binaire est donc de la forme ``110xxxxx 10xxxxxx`` ou chaque ``x`` représente un bit issu du point de code.

	Le point de code ``U+00C9`` s'écrit en binaire ``00000000~11001001``. On remplace chaque bit ``x`` par chaque bit de ``U+00C9`` en partant de la droite comme le montre le tableau ci-dessous.

	============ = = = = = = = = = = = = = = = =
	Sur 2 octets 1 1 0 x x x x x 1 0 x x x x x x
	============ = = = = = = = = = = = = = = = =
	U+00C9             0 0 0 1 1     0 0 1 0 0 1
	UTF-8        1 1 0 0 0 0 1 1 1 0 0 0 1 0 0 1
	============ = = = = = = = = = = = = = = = =

	Le caractère "É" de point de code ``U+00C9`` est encodé ``11000011 10001001`` en UTF-8 soit ``C3 89`` en hexadécimal.